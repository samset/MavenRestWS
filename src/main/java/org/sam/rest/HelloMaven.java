package org.sam.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloMaven {
	@GET
	public Response getMsg()
	{
		String output = "Hello";
		return Response.status(200).entity(output).build();
	}
}

